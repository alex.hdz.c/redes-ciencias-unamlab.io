# IP - Internet Protocol

## Referencias

### Cabeceras de TCP e IP

+ <https://www.redbooks.ibm.com/pubs/pdfs/redbooks/gg243376.pdf>
+ <https://nmap.org/book/tcpip-ref.html>
+ <https://www.sans.org/security-resources/tcpip.pdf>
+ <https://www.cs.nmt.edu/~risk/TCP-UDP%20Pocket%20Guide.pdf>

### Subredes de IPv4 e IPv6

+ <https://linux.die.net/man/1/ipcalc>
+ <http://www.gestioip.net/cgi-bin/subnet_calculator.cgi>
+ <http://eecs.ucf.edu/~aeisler/ccna_prep/Mastering%20IP%20Subnetting%20Forever%21.pdf>
+ <https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.1.0/com.ibm.zos.v2r1.hald001/subnetting.htm>
+ <https://www.cisco.com/c/en/us/support/docs/ip/routing-information-protocol-rip/13788-3.html>
+ <http://www.routeralley.com/guides/ipv4.pdf>
+ <https://www.tutorialspoint.com/ipv4/ipv4_subnetting.htm>
+ <http://packetlife.net/media/library/15/IPv4_Subnetting.pdf>
+ <https://learningnetwork.cisco.com/docs/DOC-30361>
+ <https://technet.microsoft.com/en-us/library/bb962065.aspx>
+ <https://subnettingpractice.com/howtoipv6.html>
+ <https://techprepnwo.org/wp-content/uploads/2015/06/IPv6-Addressing-and-Subnetting-Workbook-Instructors-Version.pdf>
